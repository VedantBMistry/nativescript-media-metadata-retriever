# nativescript-media-metadata-retriever
NativeScript plugin for Android to extract the metadata from a media file.

## Build

Run the create script in the root of the repo:
```
./create.sh
```
This will create a `nativescript-media-metadata-retriever-*.tgz` file (* stands for version) within ./dist folder.

Then add the plugin to your project using `tns plugin add <path-to-the-generated-tgz-file>`
